'use strict';

const express = require('express');
const os = require('os');
const uuid = require('uuid/v4');
const pino = require('pino');
const pinoExpress = require('express-pino-logger');
const logger = pino({
  level: process.env.LOG_LEVEL || 'info'
});
const loggerExpress = pinoExpress(logger);
var PropertiesReader = require('properties-reader');
var isAlive = true


// Constants
const PORT = process.env.PORT || 8080;
const HOST = '0.0.0.0';
const hostname = os.hostname();
const config = process.env.CONFIG
var properties;

if (config == undefined) {
  logger.info('Use default config file from config/app.ini');
  properties = PropertiesReader('config/app.ini');

} else {
  logger.info('Loading config file from ' + config);
  properties = PropertiesReader(config);
}
const version = properties.get('main.version');

// App
const app = express();
// set log
app.use(loggerExpress);

app.get('/', (req, res) => {
  var message = properties.get('main.message');
  var return_status = 200;
  if (isAlive == false) {
    message = 'Service unavailable';
    logger.info('Service Unavailable, return 503');
    return_status = 503;
  } else {
    logger.info('Service Available, return 200');
  }
  res.status(return_status).json({
    version: version,
    uuid: uuid(),
    message: message,
    hostname: hostname
  });
  //req.log.info('greeting');
});

app.get('/stop', (req, res) => {
  isAlive = false;
  logger.info('stop app');
  res.status(200).json({
    version: version,
    uuid: uuid(),
    message: "App is stopped working",
    hostname: hostname
  });
  //req.log.info('App is stopped working');
});

app.get('/start', (req, res) => {
  isAlive = true;
  logger.info('start app');
  res.status(200).json({
    version: version,
    uuid: uuid(),
    message: "App is started",
    hostname: hostname
  });
  //req.log.info('App is started');
});

app.get('/status', (req, res) => {
  var status = 200;
  var message = 'OK';
  if (isAlive == false) {
    status = 503;
    message = 'Unavailable';
  }
  logger.info(`current application status: ${message}`)
  res.status(status).json({
    version: version,
    uuid: uuid(),
    message: message,
    hostname: hostname
  });
  //req.log.info('Check app status');
});

app.listen(PORT, HOST);
logger.info(`Running on http://${HOST}:${PORT}`);
