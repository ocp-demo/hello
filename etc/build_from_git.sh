#!/bin/sh
NAMESPACE=dev
APP_NAME=hello
APP_TAG=1.0.0
oc new-build nodejs~https://gitlab.com/ocp-demo/hello.git \
--context-dir=code \
--name=$APP_NAME  \
-n $NAMESPACE
oc logs bc/$APP_NAME -f -n $NAMESPACE
oc tag $APP_NAME:latest \
$APP_NAME:$APP_TAG -n $NAMESPACE
