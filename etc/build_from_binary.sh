#!/bin/sh
NAMESPACE=$1
VERSION=$2
APP_NAME=hello
NODEJS_IS=openshift/nodejs
NODEJS_TAG=latest
set -x
oc get bc $APP_NAME 1>/dev/null 2>&1
if [ $? -ne 0 ];
then
    echo "There is no build config yet. Then start create build config"
    oc new-build --binary=true --name=$APP_NAME  \
    --image-stream=$NODEJS_IS:$NODEJS_TAG \
    -n $NAMESPACE
fi
echo "Start build $APP_NAME"
oc start-build hello-js --from-dir=../code --follow -n $NAMESPACE
echo "Tag $APP_NAME with $VERSION"
#APP_TAG=$(echo $VERSION|sed s/\\./-/g)
oc tag $APP_NAME:latest \
$APP_NAME:$VERSION -n $NAMESPACE
#echo "Create new app for $APP_NAME version $VERSION"
# oc new-app $APP_NAME:$VERSION \
# --name=$APP_NAME --labels=app=$APP_NAME,version=$VERSION -n $NAMESPACE 
