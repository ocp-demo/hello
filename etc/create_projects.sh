#!/bin/sh
CICD=viz-cicd
DEV=viz-dev
oc new-project $CICD --display-name="CI/CD"
oc new-project $DEV --display-name="Development Environment"
oc policy add-role-to-user system:image-puller system:serviceaccount:$DEV:default --namespace $CICD
