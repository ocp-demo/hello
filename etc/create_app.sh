#!/bin/sh
NAMESPACE=viz-dev
oc apply -f deployment.yml -n $NAMESPACE
oc apply -f service-v1.yml -n $NAMESPACE
oc apply -f route.yml -n $NAMESPACE
