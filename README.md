# Hello Application

Hello application  is RESTful API application developed with node.js for demo Red Hat OpenShift Container Platform.

## Directory structure

Node.js code is under directory 'code' and utility scripts are under directory 'etc'. 
When deploy this application with S2I don't forget to specified context directory 'code' 

## Deployment on OpenShift

Sample shell script for deploy application on OpenShift

```

etc/build_from_binary.sh
etc/build_from_git.sh

```

## Work with config map and environment variable

This application support config map. Following are sample CLI to create config map. Remark that sample app.ini is under directory 'etc'

```

oc create configmap hello-js --from-file=app.ini

```

Application can be configured to use configmap by setting environment variable named CONFIG with absolute path to config file.

For example, If pod is configured to mount configmap at /app/config/app.ini Then set environment variable CONFIG with /app/config/app.ini

Listening port can be set by using environment variable PORT. Default is 8080 if environment variable is not set.

## API Document

Following URI are supported

| Method | URI                                   |  Description                |
|--------|---------------------------------------|-----------------------------
| GET    | /                  |  return hello message in JSON        |
| GET    | /status   |  return application status. This can be used for liveness and readiness proble       | 
| GET    | /stop                  |  Set application to return 503        |
| GET    | /start                  |  Bring application back to live     |

Sample JSON Respose message

```
{
    "hostname": "Voravits-MacBook-Pro",
    "message": "Hello, World",
    "uuid": "0cf954c1-73fc-4d27-8de4-8a42f69f71d5",
    "version": "2.0.0"
}
```

Note that attribute hostname is pod's name. Then you can use this for load balancing demo when you play with scale up.

## Configuration

Environment Variables

| Variable | Description         |Default            |
|--------|--------------------------------------|---------|
|PORT     | Port Number | 8080 |
|CONFIG   | path to config file | config/app.config|

## Logging

Application log in JSON format. Following show example of log.

Application operation

```

{
    "level": 30,
    "time": 1566908047753,
    "pid": 5042,
    "hostname": "Voravits-MacBook-Pro",
    "msg": "Use default config file from config/app.ini",
    "v": 1
}

```

Request and response message

```

{
    "level": 30,
    "time": 1566902814131,
    "pid": 91344,
    "hostname": "Voravits-MacBook-Pro",
    "req": {
        "id": 6,
        "method": "GET",
        "url": "/greeting",
        "headers": {
            "host": "localhost:8080",
            "user-agent": "HTTPie/1.0.2",
            "accept-encoding": "gzip, deflate",
            "accept": "*/*",
            "connection": "keep-alive"
        },
        "remoteAddress": "127.0.0.1",
        "remotePort": 58652
    },
    "res": {
        "statusCode": 200,
        "headers": {
            "x-powered-by": "Express",
            "content-type": "application/json; charset=utf-8",
            "content-length": "124",
            "etag": "W/\"7c-OwPJexLYfnSJH0iRUDJDX3FE40M\""
        }
    },
    "responseTime": 1,
    "msg": "request completed",
    "v": 1
}

```

## Authors

* **Voravit** 

