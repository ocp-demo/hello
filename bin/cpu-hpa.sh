#!/bin/sh
DEPLOYMENT_CONFIG=hello
PROJECT=demo
MIN=1
MAX=5
TARGET=40
oc autoscale dc/${DEPLOYMENT_CONFIG} --min ${MIN} --max ${MAX} --cpu-percent=${TARGET} -n ${PROJECT}
watch oc get hpa/${DEPLOYMENT_CONFIG} -n ${PROJECT}
