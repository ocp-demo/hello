#!/bin/bash
CONFIGMAP=hello
DEPLOYMENT=hello
PROJECT=demo
oc set volume dc/${DEPLOYMENT} --add --name=app-config \
--type=configmap --configmap-name=${CONFIGMAP} --mount-path=/app/config \
-n ${PROJECT}
#oc set env dc/${DEPLOYMENT} \
#--env=CONFIG=/app/config/app.ini \
#-n ${PROJECT}
