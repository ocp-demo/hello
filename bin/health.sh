#!/bin/sh
APP_NAME=hello
PROJECT=demo
#oc new-app https://gitlab.com/ocp-demo/hello.git -n demo --context-dir=code --name=${APP_NAME} -n ${PROJECT}
oc rollout pause dc/${APP_NAME} -n ${PROJECT}
oc set probe dc/${APP_NAME} --liveness --period-seconds=30 --failure-threshold=1 --initial-delay-seconds=40 --get-url=http://:8080/status -n ${PROJECT}
oc set probe dc/${APP_NAME}  --readiness --period-seconds=10 --failure-threshold=1 --initial-delay-seconds=45 --get-url=http://:8080/status -n ${PROJECT}
oc set resources dc ${APP_NAME}  --requests="cpu=10m,memory=60Mi" -n ${PROJECT}
oc set resources dc ${APP_NAME} --limits="cpu=50m,memory=100Mi" -n ${PROJECT}
oc rollout resume dc/${APP_NAME} -n ${PROJECT}
#oc expose svc/${APP_NAME} -n ${PROJECT}
