#!/bin/sh
CONFIGMAP_NAME=hello
PROJECT=demo
oc create configmap ${CONFIGMAP_NAME} \
    --from-file=./app.ini -n ${PROJECT}
